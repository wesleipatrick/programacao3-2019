<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");

$conexao = mysqli_connect( "localhost", "root", "123", "p3");

$query = "select * from contatos";

$contatos = mysqli_query($conexao, $query );

$itens = array();

while( $contato = mysqli_fetch_array( $contatos ) ){
//    var_dump( $contato );

    array_push( $itens, [
        "nome" => $contato['nome'],
        "telefone" => $contato['telefone']
    ] );

}

echo json_encode( $itens );

